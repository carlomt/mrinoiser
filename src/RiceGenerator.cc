#include "RiceGenerator.hh"

RiceGenerator::RiceGenerator(const double sigma, const double numin) : 
  fNuMin(numin),
  fSigma(sigma),
  fYmax(0.),
  fXmax(0.)
{
  const int imax = 1000;
  for(int i=0; i<imax; i++)
    {
      const double x = fXmax/imax * i;
      const double y = rice(x, fNuMin);
      if(y>fYmax)
	{
	  fYmax = y;
	}
    }
}

// double RiceGenerator::Generate() const
double RiceGenerator::Generate(std::default_random_engine& gen, const double nu) const
{
  std::uniform_real_distribution<double> ydis(0., fYmax);
  std::uniform_real_distribution<double> xdis(0., fXmax);

  while(true)
    {
      const double x = xdis(gen);
      const double y = ydis(gen);      
      if(y<rice(x, nu))
	{
	  return x;
	}
    }
}

double RiceGenerator::rice(const double x, const double nu) const
{
  const double sigma2 = fSigma*fSigma;
  return x/(sigma2) * std::exp(-(x*x + nu*nu)/(2.*sigma2)) * std::cyl_bessel_i(0, x*nu/sigma2);
}
