#include <iostream>
#include <string>

#include <opencv2/opencv.hpp>

std::string type2str(int type) 
{
  std::string r;
  
  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);
  
  switch ( depth ) 
    {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
    }
  
  r += "C";
  r += (chans+'0');
  
  return r;
};


int main(int argc, char** argv )
{
if ( argc != 3 )
  {
printf("usage: DisplayImage.out <Image_Path> sigma\n");
return -1;
}

// Load an image
cv::Mat inputImage = cv::imread(argv[1], 0);

const double sigma = stof(std::string(argv[2]));

// Go float
cv::Mat fImage;
inputImage.convertTo(fImage, CV_32F);

// FFT
std::cout << "Direct transform...\n";
cv::Mat fourierTransform;
cv::dft(fImage, fourierTransform, cv::DFT_SCALE|cv::DFT_COMPLEX_OUTPUT);

std::cout<<"typeid(fourierTransform).name(): "<<typeid(fourierTransform).name()<<std::endl;
std::cout<< "type: "<<type2str(fourierTransform.type())<<std::endl;

// Some processing
//doSomethingWithTheSpectrum();
cv::Mat mGaussian_noise = cv::Mat(fourierTransform.size(), CV_32FC2);
std::cout<<"typeid(mGaussian_noise).name(): "<<typeid(mGaussian_noise).name()<<std::endl;
std::cout<< "type: "<<type2str(mGaussian_noise.type())<<std::endl;

cv::randn(mGaussian_noise, 0, sigma);
fourierTransform += mGaussian_noise;

// IFFT
std::cout << "Inverse transform...\n";
cv::Mat inverseTransform;
cv::dft(fourierTransform, inverseTransform, cv::DFT_INVERSE|cv::DFT_REAL_OUTPUT);

// Back to 8-bits
cv::Mat finalImage;
inverseTransform.convertTo(finalImage, CV_8U);

cv::imshow("Input Image"       , inputImage  );    // Show the result
cv::imshow("Output Image U8"   , finalImage  ); 
cv::waitKey();
return 0;
}
