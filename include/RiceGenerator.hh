#include <cmath>
#include <random>

class RiceGenerator
{
public:
  RiceGenerator(const double sigma, const double numin=0.);

  double Generate(std::default_random_engine& rng, const double nu) const;
  
private:
  const double fNuMin;
  const double fSigma;
  double fYmax;
  double fXmax;

  double rice(const double x, const double nu) const;
};
